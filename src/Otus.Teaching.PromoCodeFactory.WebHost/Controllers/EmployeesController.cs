﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            _employeeRepository.DeleteById(id);
            return Ok();
        }
        
        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employerUpdate"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployeeByIdAsync(Guid id , EmployerUpdate employerUpdate)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            employee.FirstName = employerUpdate.FirstName;
            employee.LastName = employerUpdate.LastName;
            employee.Email = employerUpdate.Email;
            employee.AppliedPromocodesCount = employerUpdate.AppliedPromocodesCount;
            return Ok();
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="employerUpdate"></param>
        /// <returns></returns>
        [HttpPut()]
        public Task<ActionResult> CreateEmployeeAsync(EmployerUpdate employerUpdate)
        {
            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employerUpdate.FirstName,
                LastName = employerUpdate.LastName,
                Email = employerUpdate.Email,
                AppliedPromocodesCount = employerUpdate.AppliedPromocodesCount,
            };
            _employeeRepository.Create(employee);
            return Task.FromResult<ActionResult>(Ok());
        }
    }
}